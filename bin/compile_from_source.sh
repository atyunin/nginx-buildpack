#!/usr/bin/env bash

# This script builds a custom version of Nginx with PCRE and HeadersMore included
# If the ENV var's are present, it will use those versions when building the binary
# otherwise it defaults to the ones marked here.


NGINX_VERSION=${NGINX_VERSION-1.15.9}
PCRE_VERSION=${PCRE_VERSION-8.42}
HEADERS_MORE_VERSION=${HEADERS_MORE_VERSION-0.33}
SET_MISC_VERSION=${SET_MISC_VERSION-0.32}
NGX_DEVEL_VERSION=${NGX_DEVEL_VERSION-0.3.1}

compile_log="$1/nginx_buildpack_compile_log.txt"
echo "NGINX_VERSION: ${NGINX_VERSION}" >> "$compile_log"
echo "PCRE_VERSION: ${PCRE_VERSION}" >> "$compile_log"
echo "HEADERS_MORE_VERSION: ${HEADERS_MORE_VERSION}" >> "$compile_log"

nginx_tarball_url=http://nginx.org/download/nginx-${NGINX_VERSION}.tar.gz
pcre_tarball_url=http://downloads.sourceforge.net/project/pcre/pcre/${PCRE_VERSION}/pcre-${PCRE_VERSION}.tar.bz2
headers_more_nginx_module_url=https://github.com/agentzh/headers-more-nginx-module/archive/v${HEADERS_MORE_VERSION}.tar.gz
set_misc_nginx_module_url=https://github.com/openresty/set-misc-nginx-module/archive/v${SET_MISC_VERSION}.tar.gz
ngx_devel_kit_url=https://github.com/vision5/ngx_devel_kit/archive/v${NGX_DEVEL_VERSION}.tar.gz

temp_dir=$(mktemp -d /tmp/nginx.XXXXXXXXXX)

cd $temp_dir
echo "Temp dir: $temp_dir"

echo "Downloading $nginx_tarball_url"
curl -L $nginx_tarball_url | tar xzv

echo "Downloading $pcre_tarball_url"
(cd nginx-${NGINX_VERSION} && curl -L $pcre_tarball_url | tar xvj )

echo "Downloading $headers_more_nginx_module_url"
(cd nginx-${NGINX_VERSION} && curl -L $headers_more_nginx_module_url | tar xvz )

echo "Downloading $set_misc_nginx_module_url"
(cd nginx-${NGINX_VERSION} && curl -L $set_misc_nginx_module_url | tar xvz )

echo "Downloading $ngx_devel_kit_url"
(cd nginx-${NGINX_VERSION} && curl -L $ngx_devel_kit_url | tar xvz )

(
	cd nginx-${NGINX_VERSION}
	./configure \
		--with-pcre=pcre-${PCRE_VERSION} \
		--prefix=/tmp/nginx \
		--add-module=${temp_dir}/nginx-${NGINX_VERSION}/headers-more-nginx-module-${HEADERS_MORE_VERSION} \
		--add-module=${temp_dir}/nginx-${NGINX_VERSION}/ngx_devel_kit-${NGX_DEVEL_VERSION} \
		--add-module=${temp_dir}/nginx-${NGINX_VERSION}/set-misc-nginx-module-${SET_MISC_VERSION} \
    --with-http_v2_module \
    --with-http_ssl_module \
    --with-http_stub_status_module \
    --with-http_sub_module \
    --with-http_gunzip_module \
    --with-http_random_index_module \
    --with-http_realip_module
	make install
)
